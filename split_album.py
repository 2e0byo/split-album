#!/usr/bin/python3

import csv
import re
import unicodedata
from argparse import ArgumentParser
from pathlib import Path
from subprocess import run
from tempfile import TemporaryDirectory

import music_tag
import taglib
from fuzzywuzzy import fuzz
from pydub import AudioSegment, silence

from datastructures import song_segment

parser = ArgumentParser()
parser.add_argument("SRC", help="Source file")
parser.add_argument(
    "--album",
    help="Album name.  If not given, we try to guess, probably not very well.",
)
parser.add_argument("--labels", help="Labels")
parser.add_argument(
    "--bounds", help="Bounds to for window to search for crossing", default="2"
)
parser.add_argument("--outdir", help=f"Outdir.  Default is constructed")

args = parser.parse_args()

audiof = Path(args.SRC).expanduser()

bounds = int(args.bounds)


def unicode_to_ascii(data, cleanup: bool = True):
    """Convert unicode data to ascii for string comparisons."""
    if not data:
        return None
    normal = unicodedata.normalize("NFKD", data).encode("ASCII", "ignore")
    val = normal.decode("utf-8")
    if len(val.strip()) == 0:
        return data  # handle cyrillic
    if cleanup:
        val = val.lower()
        # remove special characters
        val = re.sub("[^A-Za-z0-9 ]+", " ", val)
        # remove multiple spaces
        val = re.sub(" +", " ", val)
    return val


def validate_outdir():
    """Check outdir is sensible and make if need be."""
    if not args.outdir:
        args.outdir = audiof.stem
    outdir = Path(args.outdir)
    if outdir.exists():
        if outdir.is_dir():
            print("Warning! Output directory already exists")
        else:
            raise ValueError("Supplied Outdir exists and is not a dir")
    else:
        outdir.mkdir()
    return outdir


def convert_timestamp_to_s(timestamp: str):
    """
    Convert timestamp to s.

    Parameters
    ----------
    timestamp : str: [h]:m:s into s


    Returns
    -------
    seconds, as int
    """
    try:
        h, m, s = timestamp.split(":")
        return int(h) * 3600 + int(m) * 60 + int(s)
    except ValueError:
        m, s = timestamp.split(":")
        return int(m) * 60 + int(s)


def generate_segment(target: song_segment, tmpdir: str):
    """
    Generate segment file, using audio passthrough to make it really quick.

    Parameters
    ----------
    target: song_segment : a datastructures.song_segment() object representing where we _think_ the track is.

    tmpdir: Path : the directory to save the segment file in


    Returns
    -------

    A pathlib.Path() object pointing at the segment file.
    """

    lower_bound = str(target.end() - bounds)
    fn = Path(f"{tmpdir}/{target.tracknumber}{audiof.suffix}")
    cmd = [
        "ffmpeg",
        "-ss",
        lower_bound,
        "-t",
        str(bounds * 2),
        "-i",
        audiof,
        "-c:a",
        "copy",
        fn,
    ]
    print(cmd)
    run(cmd)
    return fn


def best_guess_crossing(guess_segment: AudioSegment):
    """
    Try to guess the end of this track and beginning of the next.

    Parameters
    ----------

    guess_segment : AudioSegment : pydub's representation of the
                                   segment to be analysed.

    Returns
    -------

    The end of the current track, and the beginning of the next track,
    relative to the start of this segment.
    """
    found = False
    silence_lengths = [4000, 2000, 1000, 500, 300, 200, 100, 50]
    while not found:
        for l in silence_lengths:
            print(f"Trying silence of length {l}")
            split = silence.split_on_silence(
                guess_segment, min_silence_len=l, silence_thresh=-80
            )
            if split:
                found = True
                break
        break
    if not found:
        raise ValueError("Silence not found!")

    if len(split) > 2:
        print(f"!!!warning: found multiple ({len(split)}) splits, using first and last")
    return len(split[0]) / 1000, len(split[-1]) / 1000


outdir = validate_outdir()

targets = []
if args.labels:
    with Path(args.labels).expanduser().open() as f:
        tracknumber = 0
        reader = csv.reader(f)
        for row in reader:
            tracknumber += 1
            rough_start = convert_timestamp_to_s(row[0])
            title = row[1]
            targets.append(song_segment(title, tracknumber, rough_start=rough_start))
else:
    song = taglib.File(str(audiof.resolve()))
    tracknumber = 0
    for line in song.tags["DESCRIPTION"][0].split("\n"):
        results = re.findall("([0-9]*\:*[0-9]+\:[0-9]+) +(.+)", line)
        if results:
            tracknumber += 1
            rough_start = convert_timestamp_to_s(results[0][0])
            title = results[0][1]
            targets.append(song_segment(title, tracknumber, rough_start=rough_start))

if not args.album:
    song = taglib.File(str(audiof.resolve()))
    print(song.tags)
    for char in ["-", "="]:
        title = song.tags["TITLE"][0]
        if char in title:
            parts = title.split(char)
            for part in parts:
                bad_regexes = [" *hd *", " *audio *"]
                for rgx in bad_regexes:
                    if re.findall(rgx, part.lower()):
                        parts.remove(part)
                        break

            unicode_artist = unicode_to_ascii(song.tags["ARTIST"][0])
            for part in parts:
                # try to guess which if any is artist
                unicode_part = unicode_to_ascii(part)
                ratio = fuzz.partial_ratio(unicode_artist, unicode_part)
                if ratio > 0.8:
                    parts.remove(part)

            if len(parts) > 1:
                print("Unable to narrow down with this char")
            if len(parts) == 1:
                args.album = parts[0].strip()
                break
    if not args.album:
        raise ValueError("Unable to deduce album, please enter manually.")

with TemporaryDirectory() as tmpdir:
    for i, target in enumerate(targets):
        try:
            target.rough_end = targets[i + 1].start()
        except IndexError:
            song = taglib.File(str(audiof.resolve()))
            target.rough_end = song.length
        target.tmpfile = generate_segment(target, tmpdir)

    # get starting point of first song
    target = targets[0]
    rough_start = target.start() if target.start() > bounds else bounds
    start_song_segment = song_segment("", 0, rough_end=rough_start)
    tmpfile = generate_segment(start_song_segment, tmpdir)
    segment = AudioSegment.from_file(tmpfile)
    end, start = best_guess_crossing(segment)
    target.real_start = rough_start - bounds * 2 + start

    print(f"Tempdir is: {tmpdir}")

    for i, target in enumerate(targets):
        print(f"Finding crossing for {target.title}")
        segment = AudioSegment.from_file(target.tmpfile)
        end, start = best_guess_crossing(segment)
        target.real_end = target.rough_end - bounds * 2 + end
        try:
            targets[i + 1].real_start = target.rough_end - bounds * 2 + start
        except IndexError:
            pass

for target in targets:
    cmd = ["ffmpeg"]
    cmd += ["-ss", str(target.start()), "-to", str(target.end())]
    outf = outdir / f"{target.title}{audiof.suffix}"
    print(outf)
    cmd += ["-i", audiof, "-c:a", "copy", outf]
    run(cmd)

    f = music_tag.load_file(outf)
    f["tracktitle"] = target.title
    f["album"] = args.album
    f["tracknumber"] = str(target.tracknumber)
    f.save()
