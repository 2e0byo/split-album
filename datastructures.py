#!/usr/bin/python3
from pathlib import Path
from dataclasses import dataclass

@dataclass
class song_segment():
    """Class to represent song segment with all the required properties"""
    title: str
    tracknumber: int
    tmpfile: Path = None
    rough_start: int = None
    real_start: int = None
    rough_end: int = None
    real_end: int = None
    
    def start(self):
        return self.real_start if self.real_start else self.rough_start

    def end(self):
        return self.real_end if self.real_end else self.rough_end
       
